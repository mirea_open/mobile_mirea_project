package ru.mirea.vidyakin_i_n.mireaproject;

import static android.Manifest.permission.FOREGROUND_SERVICE;
import static android.Manifest.permission.POST_NOTIFICATIONS;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import ru.mirea.vidyakin_i_n.mireaproject.databinding.FragmentAudioBinding;

public class Audio extends Fragment {

    private int PermissionCode = 200;
    private FragmentAudioBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final int[] flag = {0};
        binding = FragmentAudioBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        if (ContextCompat.checkSelfPermission(getContext(), POST_NOTIFICATIONS) == PackageManager.PERMISSION_GRANTED) {

            Log.d(MainActivity.class.getSimpleName().toString(), "Разрешения получены");
        } else {
            Log.d(MainActivity.class.getSimpleName().toString(), "Нет разрешений!");

            ActivityCompat.requestPermissions(getActivity(), new String[]{POST_NOTIFICATIONS, FOREGROUND_SERVICE}, PermissionCode);

        }
        binding.imageButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag[0] == 0)
                {
                    flag[0] = 1;
                    Intent serviceIntent = new Intent(getActivity(), PlayerService.class);
                    getActivity().startForegroundService(serviceIntent);
                    binding.textView.setText("Мемная папка - Девачбка Венсдей");
                    binding.imageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.mipmap.wed, null));
                    binding.imageButton1.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.baseline_pause_24, null));
                }
                else
                {
                    flag[0] = 0;
                    getActivity().stopService(
                            new Intent(getActivity(), PlayerService.class));
                    binding.textView.setText("Сейчас ничего не играет...");
                    binding.imageView.setImageDrawable(null);
                    binding.imageButton1.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.baseline_play_arrow_24, null));
                }

            }
        });

        return root;
    }
}