package ru.mirea.vidyakin_i_n.mireaproject;


import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.content.Context;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.mirea.vidyakin_i_n.mireaproject.databinding.FragmentCompassBinding;

public class CompassFragment extends Fragment implements SensorEventListener{

    private FragmentCompassBinding binding;
    private TextView textView;
    private float[] Gravity = new float[3];
    private float[] Geomagnetic = new float[3];
    private float azimuth = 0f;
    private float currazimuth = 0f;
    private SensorManager sensorManager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentCompassBinding.inflate(inflater, container, false);
        textView = binding.textView;
        sensorManager =
                (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);

        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), sensorManager.SENSOR_DELAY_GAME);
        sensorManager.registerListener(this,sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), sensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        final float alpha = 0.97f;
        synchronized (this) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            {
                Gravity[0] = alpha*Gravity[0]+(1-alpha)*event.values[0];
                Gravity[1] = alpha*Gravity[1]+(1-alpha)*event.values[1];
                Gravity[2] = alpha*Gravity[2]+(1-alpha)*event.values[2];
            }
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            {
                Geomagnetic[0] = alpha*Geomagnetic[0]+(1-alpha)*event.values[0];
                Geomagnetic[1] = alpha*Geomagnetic[1]+(1-alpha)*event.values[1];
                Geomagnetic[2] = alpha*Geomagnetic[2]+(1-alpha)*event.values[2];
            }
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R,I,Gravity,Geomagnetic);
            if (success)
            {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R,SensorManager.getOrientation(R,orientation));
                azimuth = (float)Math.toDegrees(orientation[0]);
                azimuth+=360;
                azimuth%=360;
                String where = "NW";
                if (azimuth >= 350 || azimuth <= 10)
                    where = "N";
                if (azimuth < 350 && azimuth > 280)
                    where = "NW";
                if (azimuth <= 280 && azimuth > 260)
                    where = "W";
                if (azimuth <= 260 && azimuth > 190)
                    where = "SW";
                if (azimuth <= 190 && azimuth > 170)
                    where = "S";
                if (azimuth <= 170 && azimuth > 100)
                    where = "SE";
                if (azimuth <= 100 && azimuth > 80)
                    where = "E";
                if (azimuth <= 80 && azimuth > 10)
                    where = "NE";
                currazimuth = azimuth;
                textView.setText(where);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}