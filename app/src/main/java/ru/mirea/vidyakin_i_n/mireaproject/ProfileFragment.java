package ru.mirea.vidyakin_i_n.mireaproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.mirea.vidyakin_i_n.mireaproject.databinding.FragmentProfileBinding;

public class ProfileFragment extends Fragment {

    FragmentProfileBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater, container, false);
        Button button = binding.button1;
        EditText editText1 = binding.editText1;
        EditText editText2 = binding.editText2;
        EditText editText3 = binding.editText3;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = getActivity().getSharedPreferences("Profile", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("Name", editText1.getText().toString());
                editor.putInt("Age", Integer.parseInt(editText2.getText().toString()));
                editor.putString("City", editText3.getText().toString());
                editor.apply();
                Toast.makeText(getActivity(),sharedPref.getString("Name", "")+"|"+String.valueOf(sharedPref.getInt("Age", 0))+"|"+sharedPref
                        .getString("City", ""), Toast.LENGTH_LONG).show();
            }
        });
        return binding.getRoot();
    }
}