package ru.mirea.vidyakin_i_n.mireaproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ru.mirea.vidyakin_i_n.mireaproject.databinding.FragmentFilesBinding;
import ru.mirea.vidyakin_i_n.mireaproject.databinding.FragmentProfileBinding;

public class FilesFragment extends Fragment {
    FragmentFilesBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFilesBinding.inflate(inflater, container, false);
        Button button1 = binding.buttoncode;
        Button button2 = binding.buttondecode;
        EditText editText1 = binding.editTextName;
        EditText editText2 = binding.editTextPassword;
        SharedPreferences sharedPref = getActivity().getSharedPreferences("Account", Context.MODE_PRIVATE);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = editText1.getText().toString();
                name = CodeText(name);
                String password = editText2.getText().toString();
                password = CodeText(password);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("Name", name);
                editor.putString("Password", password);
                editor.apply();
                Toast.makeText(getActivity(), name + " " + password, Toast.LENGTH_LONG).show();
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText1.setText(CodeText(sharedPref.getString("Name", "")));
                editText2.setText(CodeText(sharedPref.getString("Password", "")));
            }
        });
        return binding.getRoot();
    }
    private String CodeText(String s)
    {
        String ss = "";
        for (int i = 0; i<s.length(); i++)
        {
            char c = s.charAt(i);
            int k = (int) c;
            k = k ^ 22;
            c = (char) k;
            ss+=c;
        }
        return  ss;
    }
}